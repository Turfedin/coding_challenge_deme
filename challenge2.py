#!/usr/bin/python

cardinals = {
   'N' : (0, 1),
   'E' : (1, 0),
   'S' : (0, -1),
   'W' : (-1, 0)
}

# Read a line, return a list of all inputs that were separated by a space
def getline():
   line = input()
   return line.split(' ')

def computePosition(directions):
   x = 0
   y = 0
   for i in directions:
      direction = cardinals[i]
      x1, y1 = direction
      x += x1
      y += y1

   return x , y


def main():
   nbTestCases = int(input())
   for i in range (1, nbTestCases + 1):
      directions = getline()
      finalPosition = computePosition(directions[1:])
      print(i, finalPosition[0], finalPosition[1])

if __name__ == "__main__":
   main()
