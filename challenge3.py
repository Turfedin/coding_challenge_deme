#!/usr/bin/python

first15 = 250
second15 = 275
third15 = 300
other15 = 325

# Read 2 lines
def getline():
   _ = input()
   line2 = input()
   line2 = list(map(int, line2.split(' ')))

   return line2

def get_consumption(duration):
   bucket = [duration, 0, 0, 0]

   for i in range (0, 3):
      if bucket[i] > 15:
         bucket[i] = 15
         duration = duration - 15
         bucket[i + 1] = duration

   return bucket[0] * first15 + bucket[1] * second15 + bucket[2] * third15 + bucket[3] * other15

def main():
   nbTestCases = int(input())
   for i in range (1, nbTestCases + 1):
      dredgings = getline()
      consumption = 0
      for j in dredgings:
         consumption += get_consumption(j)
      
      print(i, consumption)

if __name__ == "__main__":
   main()