import itertools
import sys

def find_all_possible_combinations(n,m):
    combinations = []


    stuff = []
    for i in range(n-1):
        stuff.append(0)
    for i in range(m-1):
        stuff.append(1)

    for L in range(0, len(stuff)+1):
        for subset in itertools.permutations(stuff, L):
            if len(subset) == (n + m - 2):
                
                if subset not in combinations:
                    combinations.append(subset)

    return combinations


def find_all_possible_paths(combinations, matrix, totalCost):
    no_paths = 0

    for combination in combinations:
        startValue = matrix[0][0]
        index = (0,0)
        for d in combination:
            if d == 1:
                
                index = (index[0], index[1] + 1)
                startValue += matrix[index[0]][index[1]]

            if d == 0:
                index = (index[0]+1, index[1])
                startValue += matrix[index[0]][index[1]]
        
        if startValue == int(totalCost):
            no_paths += 1   
    
    return no_paths

def getline():
    args = []
    line2 = input() #cost
    args.append(line2)
    line3 = input()  #n,m
    nm=list(map(int, line3.split(' ')))
    args.append(nm)
    rows=[]
    for i in range(0,nm[0]):
        line = input()
        row=list(map(int, line.split(' ')))
        rows.append(row)  
    args.append(rows)
    return args

def main():
    nbTestCases = int(input())

    output = []
    
    for i in range (1, nbTestCases+1):
        directions = getline()   
    
        totalCost = directions[0]
        n,m = directions[1]
        matrix = directions[2]

        combinations = find_all_possible_combinations(n,m)
        no_possible_paths = find_all_possible_paths(combinations, matrix, totalCost)
        output.append([i, no_possible_paths])
    
    for i in output:
        print(str(i[0]) + " " + str(i[1]) + "\n")




if __name__ == "__main__":
   main()