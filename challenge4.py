#!/usr/bin/python

import sys

ERROR = 0.001

# Read a line
def getline():
   duration = int(input())
   _ = input()
   line3 = input()
   line3 = list(map(float, line3.split(' ')))
   return duration, [[line3[i], line3[i+1], line3[i+2]] for i in range(0, len(line3), 3)]

def getMaxProduction(duration, triples):
   maxTimes = [0, 0]
   liveProduction = [0.0 for i in range(0, duration + 1)]

   for t in triples:
      start = int(t[0])
      end = int(t[1])
      for i in range(start, end+1):
         liveProduction[i] += t[2]
         liveProduction[i] = round(liveProduction[i], 1)

   maxProd = max(liveProduction)
   foundMax = False

   for i, p in enumerate(liveProduction):
      if p > maxProd - ERROR and p < maxProd + ERROR:
         if not foundMax:
            foundMax = True
            maxTimes[0] = i
      elif foundMax:
         maxTimes[1] = i - 1
         break

      if maxTimes[1] > 0.0 - ERROR and maxTimes[1] < maxProd + 0.0:
         maxTimes[1] = duration

   return maxTimes[0], maxTimes[1], maxProd

def main():
   nbTestCases = int(input())
   duration = []
   triples = []
   for _ in range(1, nbTestCases + 1):
      duration_k, triples_k = getline()
      
      duration.append(duration_k)
      triples.append(triples_k)

   for j in range(1, nbTestCases + 1):
      begin, end, maxProduction = getMaxProduction(duration[j-1], triples[j-1])
      print(j, begin, end, maxProduction)

if __name__ == "__main__":
   main()
